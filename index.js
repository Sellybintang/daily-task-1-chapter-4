// http modul yang diimport atau panggil
const http = require('http');

// dotenv modul (third party modul) yang diimport atau panggil
require ('dotenv').config();
const PORT = process.env.PORT ;

// folder publik
const fs = require ('fs');
const path = require ('path');
const PUBLIC_DIRECTORY = path.join (__dirname,'publik');


// import data
const data = require ('./datasource');
  // const dataJSON = fs.readFileSync (path.normalize('datasource.js')); 
// console.log(dataJSON);
  // const dataArray = JSON.parse(dataJSON);
// console.log(dataArray);
  // const {datasource, datasource1} =require("./data_a");
const datab = require ('./data_b');
const dataa = require ('./data_a');



// ubah value ke string JSON
function toJSON(value) {
  return JSON.stringify(value);
}

function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, 'utf-8')
}

function onRequest(req, res) {
  switch(req.url) {
    case "/":
      res.writeHead(200)
      res.end(getHTML("index.html"))
      return;


    case "/about":
      res.writeHead(200)
      res.end(getHTML("about.html"))
      return;

    case "/data":
      const DataSource = toJSON(dataJSON);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(DataSource);
      return;

    case "/dataa":
      const DataSource1 = toJSON(dataa);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(DataSource1);
      return;

    case "/datab":
      const DataSource2 = toJSON(datab);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(DataSource2);
      return;
      
    default:
      res.writeHead(404)
      res.end(getHTML("404.html"))
      return;
  }
}

 
 const server = http.createServer(onRequest);
 
 // Jalankan server
 server.listen(PORT, '0.0.0.0', () => {
   console.log("Server sudah berjalan, silahkan buka http://localhost:%d", PORT);
 })